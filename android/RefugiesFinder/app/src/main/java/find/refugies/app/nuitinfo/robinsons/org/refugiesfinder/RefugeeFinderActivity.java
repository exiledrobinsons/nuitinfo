package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.Camp;
import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.Refugee;
import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.RefugeeListAdapter;
import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.Sex;

public class RefugeeFinderActivity extends Activity {

    private ListView refugeeList;
    private RefugeeListAdapter arrayAdapter;
    private RefugeeSearchQuery lastQuery = new RefugeeSearchQuery();
    private boolean runningQuery;
    private ImageView searchButton;
    private Spinner originSpinner;
    private Spinner countrySpinner;
    private EditText name;
    private List<Long> favorites;

    private static final String url = "http://54.154.5.42:8081/rest/robinson/refugee";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refugee_finder);

        getActionBar().setHomeButtonEnabled(true);

        refugeeList = (ListView) findViewById(R.id.find_list_refugee);
        arrayAdapter = new RefugeeListAdapter(this, R.layout.refugee_list_layout, new LinkedList<Refugee>());
        refugeeList.setAdapter(arrayAdapter);
        refugeeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Refugee refugee = (Refugee) parent.getItemAtPosition(position);
                Intent i = new Intent(RefugeeFinderActivity.this, RefugeeView.class);
                i.putExtra("refugee", new Gson().toJson(refugee));
                i.putExtra("favorite", favorites.contains(refugee.getId()));
                startActivity(i);
            }
        });
        originSpinner = (Spinner) findViewById(R.id.find_origin_spinner);
        countrySpinner = (Spinner) findViewById(R.id.find_country_spinner);
        name = (EditText) findViewById(R.id.find_edit_search);

        searchButton = (ImageView) findViewById(R.id.find_search_img);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!runningQuery) {
                    RefugeeSearchQuery query = new RefugeeSearchQuery(name.getText().toString(), countrySpinner.getSelectedItem().toString(), originSpinner.getSelectedItem().toString());
                    if (!lastQuery.equals(query)) {
                        invokeWS(query);
                        lastQuery = query;
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Type listType = new TypeToken<List<Long>>() {
        }.getType();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        String fav = prefs.getString("favorites", null);
        favorites = new LinkedList<Long>();
        if (fav != null)
            favorites = new Gson().fromJson(fav, listType);
        invokeWS(new RefugeeSearchQuery());
    }

    @Override
    protected void onPause() {
        lastQuery = null;
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refugee_finder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param query
     */
    public void invokeWS(RefugeeSearchQuery query) {
        RequestParams params = new RequestParams();
        params.put("name", query.getName());

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onFinish() {
                super.onFinish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    List<Refugee> refugees = new LinkedList<Refugee>();
                    String str = new String(responseBody);
                    Log.w("LOL", str);
                    // JSON Object
                    JSONArray array = new JSONArray(str);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject o = array.getJSONObject(i);
                        JSONObject c = o.getJSONObject("camp");
                        Sex sex;
                        if (o.has("sexe"))
                            sex = o.get("sexe").equals("MALE") ? Sex.MALE : Sex.FEMALE;
                        else
                            sex = Sex.MALE;
                        Camp camp = new Camp(c.getString("name"), c.getDouble("longitude"), c.getDouble("latitude"), c.getString("country"));
                        Refugee r = new Refugee(o.getString("firstname"), o.getString("lastname"),
                                o.getString("homeCountry"), sex, camp, o.getLong("id"));
                        r.setFavorite(favorites.contains(r.getId()));
                        refugees.add(r);
                    }
                    arrayAdapter.setValues(refugees);
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Error Occured!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(getApplicationContext(), "Network error while requesting database.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
