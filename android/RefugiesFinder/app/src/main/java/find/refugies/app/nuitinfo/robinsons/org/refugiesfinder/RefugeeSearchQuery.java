package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder;

public class RefugeeSearchQuery {

    private String name = "";
    private String location = "";
    private String origin = "";

    public RefugeeSearchQuery() {
    }

    public RefugeeSearchQuery(String name, String location, String origin) {
        this.name = name;
        this.location = location;
        this.origin = origin;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getOrigin() {
        return origin;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RefugeeSearchQuery)) return false;
        RefugeeSearchQuery q = (RefugeeSearchQuery) o;
        if (q == null && this != null) return false;
        return q.getLocation().equals(location) && q.getName().equals(name) && q.getOrigin().equals(origin);
    }

}
