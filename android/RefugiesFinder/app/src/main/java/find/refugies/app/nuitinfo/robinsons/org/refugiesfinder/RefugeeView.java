package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.Refugee;
import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data.Sex;

public class RefugeeView extends Activity {

    private Refugee refugee;
    private ImageView favoriteImg;
    private boolean isFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refugee_view);

        isFavorite = getIntent().getBooleanExtra("favorite", false);
        String s = getIntent().getStringExtra("refugee");
        refugee = new Gson().fromJson(s, Refugee.class);

        ((TextView) findViewById(R.id.refugee_desc)).setText(refugee.getFirstName() + " " + refugee.getLastName());
        ((TextView) findViewById(R.id.camp_desc)).setText(refugee.getCamp().getName() + " - " + refugee.getCamp().getCountry());
        ImageView icon = (ImageView) findViewById(R.id.icon);
        if (refugee.getSex().equals(Sex.FEMALE))
            icon.setImageResource(R.drawable.female);
        else
            icon.setImageResource(R.drawable.male);
        favoriteImg = (ImageView) findViewById(R.id.icon_favorite);
        if (isFavorite)
            favoriteImg.setImageResource(R.drawable.favorite);
        favoriteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type listType = new TypeToken<List<Long>>() {
                }.getType();
                SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
                String fav = prefs.getString("favorites", null);
                List<Long> favorites = new LinkedList<Long>();
                if (fav != null)
                    favorites = new Gson().fromJson(fav, listType);
                if (isFavorite) {
                    favorites.remove(refugee.getId());
                    favoriteImg.setImageResource(R.drawable.favorite_add);
                } else {
                    favorites.add(refugee.getId());
                    favoriteImg.setImageResource(R.drawable.favorite);
                }
                isFavorite = !isFavorite;
                prefs.edit().putString("favorites", new Gson().toJson(favorites, listType)).commit();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refugee_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
