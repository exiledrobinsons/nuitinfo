package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data;

public class Refugee {

    private String firstName;
    private String lastName;
    private String originCountry;
    private Camp camp;
    private Sex sex;
    private long id;
    private boolean favorite;

    public Refugee(String firstName, String lastName, String originCountry, Sex sex, Camp camp, long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.originCountry = originCountry;
        this.camp = camp;
        this.sex = sex;
        this.id = id;
    }

    public Camp getCamp() {
        return camp;
    }
    public String getOriginCountry() {
        return originCountry;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public Sex getSex() {
        return sex;
    }
    public long getId() {
        return id;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
