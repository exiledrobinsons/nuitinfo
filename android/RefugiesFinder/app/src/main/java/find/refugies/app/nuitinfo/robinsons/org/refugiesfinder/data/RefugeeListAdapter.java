package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.R;

public class RefugeeListAdapter extends ArrayAdapter<Refugee> {

    private final Activity context;
    private List<Refugee> values;
    private final int layout;

    public RefugeeListAdapter(Activity context, int layout, List<Refugee> values) {
        super(context, layout, values);
        this.context = context;
        this.layout = layout;
        this.values = values;
    }

    public void setValues(List<Refugee> values) {
        this.clear();
        for (Refugee r : values) {
            this.add(r);
        }
        this.values = values;
        this.notifyDataSetChanged();
    }

    static class ViewHolder {
        public TextView firstLine;
        public TextView secondLine;
        public ImageView image;
        public ImageView imageFav;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder = null;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(layout, null);
            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.firstLine = (TextView) rowView.findViewById(R.id.firstLine);
            viewHolder.secondLine = (TextView) rowView.findViewById(R.id.secondLine);
            viewHolder.image = (ImageView) rowView
                    .findViewById(R.id.icon);
            viewHolder.imageFav = (ImageView) rowView
                    .findViewById(R.id.icon_fav);
            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        Refugee refugee = values.get(position);
        // fill data
        viewHolder.firstLine.setText(refugee.getFirstName() + " " + refugee.getLastName());
        viewHolder.secondLine.setText(refugee.getCamp().getName() + " - " + refugee.getCamp().getCountry());
        if (refugee.getSex() == Sex.MALE) {
            viewHolder.image.setImageResource(R.drawable.male);
        } else if (refugee.getSex() == Sex.FEMALE) {
            viewHolder.image.setImageResource(R.drawable.female);
        } else {
            viewHolder.image.setImageResource(R.drawable.man);
        }
        if(refugee.isFavorite()) {
            viewHolder.imageFav.setImageResource(R.drawable.favorite);
            viewHolder.imageFav.setVisibility(View.VISIBLE);
        }
        else
            viewHolder.imageFav.setVisibility(View.INVISIBLE);

        return rowView;
    }
}
