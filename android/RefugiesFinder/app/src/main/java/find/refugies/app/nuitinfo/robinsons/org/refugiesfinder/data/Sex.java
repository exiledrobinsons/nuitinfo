package find.refugies.app.nuitinfo.robinsons.org.refugiesfinder.data;

public enum Sex {
    MALE, FEMALE
}
