package org.robinsons.nuitinfo.domain.impl.finder;

import org.robinsons.nuitinfo.domain.interfaces.finder.CampFinder;
import org.robinsons.nuitinfo.domain.interfaces.finder.MissionFinder;
import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.entities.Mission;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CampFinderBean implements CampFinder {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Camp findById(long id) throws UnknownCampException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Camp> criteria = builder.createQuery(Camp.class);
        Root<Camp> from = criteria.from(Camp.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));
        TypedQuery<Camp> query = entityManager.createQuery(criteria);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            throw new UnknownCampException(""+ id);
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean exists(long id) {
        try {
            findById(id);
            return true;
        } catch (UnknownCampException e) {
            return false;
        }
    }
    /*

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findByOwner(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);
		criteria.where(builder.equal(from.get("owner").get("login"), login));
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

    @Override
    public List<Channel> findPublicByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<PublicChannel> from = criteria.from(PublicChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
    public List<Channel> findInHouseByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<InHouseChannel> from = criteria.from(InHouseChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
    public List<Channel> findPrivateByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<PrivateChannel> from = criteria.from(PrivateChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findByMember(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);
		criteria.where(builder.like(from.<UserChannel> get("members")
				.<String> get("userLogin"), login));
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findModerated(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);

		criteria.where(
				builder.and(
						builder.like(from.<UserChannel> get("members").<String> get("userLogin"),login),
						builder.or(
								builder.equal(from.<UserChannel> get("members").<UserRights> get("userRights"), UserRights.ADMIN),
								builder.equal(from.<UserChannel> get("members").<UserRights> get("userRights"), UserRights.MODERATE)
						)
				)
		);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

    @Override
    public List<Channel> findAdministratedChannel(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<Channel> from = criteria.from(Channel.class);
        criteria.select(from);

        criteria.where(
                builder.and(
                        builder.like(from.<UserChannel> get("members").<String> get("userLogin"),login),
                        builder.equal(from.<UserChannel>get("members").<UserRights>get("userRights"), UserRights.ADMIN)
                )
        );

        criteria.orderBy(builder.asc(from.<String> get("name")));

        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isPublic(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof PublicChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isPrivate(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof PrivateChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isInHouse(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof InHouseChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllPublic() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<PublicChannel> from = criteria.from(PublicChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllPrivate() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<PrivateChannel> from = criteria.from(PrivateChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllInHouse() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<InHouseChannel> from = criteria.from(InHouseChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public UserChannel findUserChannel(String login, String name)
			throws UnknownEntityException {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserChannel> criteria = builder
				.createQuery(UserChannel.class);
		Root<UserChannel> from = criteria.from(UserChannel.class);
		criteria.select(from);
		criteria.where(builder.and(builder.equal(from.get("userLogin"), login),
				builder.equal(from.get("channelName"), name)));

		TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			throw new UnknownUserChannelAssociationException(login, name);
		}
	}

    @Override
    public List<UserChannel> findUserChannelByUser(String login){
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserChannel> criteria = builder
                .createQuery(UserChannel.class);
        Root<UserChannel> from = criteria.from(UserChannel.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("userLogin"), login));

        criteria.orderBy(builder.asc(from.<String> get("userRights")));

        TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
    public List<UserChannel> findUserChannelByChannel(String name){
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserChannel> criteria = builder
                .createQuery(UserChannel.class);
        Root<UserChannel> from = criteria.from(UserChannel.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("channelName"), name));

        criteria.orderBy(builder.asc(from.<String> get("userRights")));

        TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
	public UserRights findUserRights(String login, String name)
			throws UnknownEntityException {
		UserChannel uc = findUserChannel(login, name);
		return uc.userRights;
	}
	*/
}
