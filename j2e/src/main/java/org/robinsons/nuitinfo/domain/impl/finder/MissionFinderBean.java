package org.robinsons.nuitinfo.domain.impl.finder;

import org.robinsons.nuitinfo.domain.interfaces.finder.MissionFinder;
import org.robinsons.nuitinfo.entities.Mission;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Benjamin Bourgeois on 04/12/2014 22:53.
 */

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MissionFinderBean implements MissionFinder {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Mission findByName(String n)  {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Mission> criteria = builder.createQuery(Mission.class);
        Root<Mission> from = criteria.from(Mission.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("name"), n));
        TypedQuery<Mission> query = entityManager.createQuery(criteria);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Mission> getAllMission() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Mission> cq = cb.createQuery(Mission.class);
        TypedQuery<Mission> allQuery = entityManager.createQuery(cq.select(cq.from(Mission.class)));
        return allQuery.getResultList();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean exists(String name) {
        return findByName(name)!=null;
    }
    /*

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean exists(String name) {
		try {
			findByName(name);
			return true;
		} catch (UnknownChannelException e) {
			return false;
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findByOwner(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);
		criteria.where(builder.equal(from.get("owner").get("login"), login));
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

    @Override
    public List<Channel> findPublicByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<PublicChannel> from = criteria.from(PublicChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
    public List<Channel> findInHouseByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<InHouseChannel> from = criteria.from(InHouseChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
    public List<Channel> findPrivateByMember(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<PrivateChannel> from = criteria.from(PrivateChannel.class);
        criteria.select(from);
        criteria.where(builder.like(from.<UserChannel> get("members")
                .<String> get("userLogin"), login));
        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();    }

    @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findByMember(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);
		criteria.where(builder.like(from.<UserChannel> get("members")
				.<String> get("userLogin"), login));
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findModerated(String login) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<Channel> from = criteria.from(Channel.class);
		criteria.select(from);

		criteria.where(
				builder.and(
						builder.like(from.<UserChannel> get("members").<String> get("userLogin"),login),
						builder.or(
								builder.equal(from.<UserChannel> get("members").<UserRights> get("userRights"), UserRights.ADMIN),
								builder.equal(from.<UserChannel> get("members").<UserRights> get("userRights"), UserRights.MODERATE)
						)
				)
		);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);

		return query.getResultList();
	}

    @Override
    public List<Channel> findAdministratedChannel(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
        Root<Channel> from = criteria.from(Channel.class);
        criteria.select(from);

        criteria.where(
                builder.and(
                        builder.like(from.<UserChannel> get("members").<String> get("userLogin"),login),
                        builder.equal(from.<UserChannel>get("members").<UserRights>get("userRights"), UserRights.ADMIN)
                )
        );

        criteria.orderBy(builder.asc(from.<String> get("name")));

        TypedQuery<Channel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isPublic(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof PublicChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isPrivate(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof PrivateChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isInHouse(String name) throws UnknownChannelException {
		Channel c = findByName(name);
		return c instanceof InHouseChannel;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllPublic() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<PublicChannel> from = criteria.from(PublicChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllPrivate() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<PrivateChannel> from = criteria.from(PrivateChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<Channel> findAllInHouse() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Channel> criteria = builder.createQuery(Channel.class);
		Root<InHouseChannel> from = criteria.from(InHouseChannel.class);
		criteria.select(from);
		TypedQuery<Channel> query = entityManager.createQuery(criteria);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public UserChannel findUserChannel(String login, String name)
			throws UnknownEntityException {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserChannel> criteria = builder
				.createQuery(UserChannel.class);
		Root<UserChannel> from = criteria.from(UserChannel.class);
		criteria.select(from);
		criteria.where(builder.and(builder.equal(from.get("userLogin"), login),
				builder.equal(from.get("channelName"), name)));

		TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			throw new UnknownUserChannelAssociationException(login, name);
		}
	}

    @Override
    public List<UserChannel> findUserChannelByUser(String login){
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserChannel> criteria = builder
                .createQuery(UserChannel.class);
        Root<UserChannel> from = criteria.from(UserChannel.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("userLogin"), login));

        criteria.orderBy(builder.asc(from.<String> get("userRights")));

        TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
    public List<UserChannel> findUserChannelByChannel(String name){
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserChannel> criteria = builder
                .createQuery(UserChannel.class);
        Root<UserChannel> from = criteria.from(UserChannel.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("channelName"), name));

        criteria.orderBy(builder.asc(from.<String> get("userRights")));

        TypedQuery<UserChannel> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }

    @Override
	public UserRights findUserRights(String login, String name)
			throws UnknownEntityException {
		UserChannel uc = findUserChannel(login, name);
		return uc.userRights;
	}
	*/
}
