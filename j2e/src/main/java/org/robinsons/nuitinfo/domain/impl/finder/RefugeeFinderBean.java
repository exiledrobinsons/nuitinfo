package org.robinsons.nuitinfo.domain.impl.finder;

import org.robinsons.nuitinfo.domain.interfaces.finder.RefugeeFinder;
import org.robinsons.nuitinfo.entities.Refugee;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownRefugeeException;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RefugeeFinderBean implements RefugeeFinder {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Refugee findById(long id) throws UnknownRefugeeException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Refugee> criteria = builder.createQuery(Refugee.class);
        Root<Refugee> from = criteria.from(Refugee.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));
        TypedQuery<Refugee> query = entityManager.createQuery(criteria);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            throw new UnknownRefugeeException("" + id);
        }
    }

    @Override
    public Refugee findByName(String firstname, String lastname) throws UnknownRefugeeException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Refugee> criteria = builder.createQuery(Refugee.class);
        Root<Refugee> from = criteria.from(Refugee.class);
        criteria.select(from);
        criteria.where(builder.and(builder.equal(from.get("firstname"), firstname), builder.equal(from.get("lastname"), lastname)));
        TypedQuery<Refugee> query = entityManager.createQuery(criteria);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            throw new UnknownRefugeeException(lastname);
        }
    }

    @Override
    public List<Refugee> findRefugeesByName(String firstname, String lastname) throws UnknownRefugeeException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Refugee> criteria = builder.createQuery(Refugee.class);
        Root<Refugee> from = criteria.from(Refugee.class);
        criteria.select(from);
        criteria.distinct(true);

        criteria.where(builder.equal(from.<String>get("firstname"), firstname));

        /*
        //Where clause
        criteria.where(
                builder.or(
                        builder.function(
                                "CONTAINS", Boolean.class,
                                //assuming 'lastName' is the property on the Person Java object that is mapped to the last_name column on the Person table.
                                from.<String>get("firstname"),
                                //Add a named parameter called containsCondition
                                builder.parameter(String.class, firstname)
                        ),
                        builder.function(
                                "CONTAINS", Boolean.class,
                                //assuming 'lastName' is the property on the Person Java object that is mapped to the last_name column on the Person table.
                                from.<String>get("lastname"),
                                //Add a named parameter called containsCondition
                                builder.parameter(String.class, lastname)
                        )
                )
        );
*/
        criteria.orderBy(builder.desc(from.<String>get("id")));

        TypedQuery<Refugee> query = entityManager.createQuery(criteria);

        return query.getResultList();
    }


    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean exists(long id) {
        try {
            findById(id);
            return true;
        } catch (UnknownRefugeeException e) {
            return false;
        }
    }
}
