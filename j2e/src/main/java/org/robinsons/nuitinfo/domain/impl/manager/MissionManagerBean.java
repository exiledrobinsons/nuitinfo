package org.robinsons.nuitinfo.domain.impl.manager;

import org.robinsons.nuitinfo.domain.interfaces.finder.CampFinder;
import org.robinsons.nuitinfo.domain.interfaces.manager.MissionManager;
import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.entities.CampMission;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.GregorianCalendar;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MissionManagerBean implements MissionManager {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private CampFinder campFinder;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public CampMission createCampMission(String name, String missionDetails, GregorianCalendar startDate, GregorianCalendar endDate, long campId) throws UnknownCampException {
        Camp camp = campFinder.findById(campId);
        CampMission c = new CampMission(name, missionDetails, startDate, endDate, camp);
        entityManager.persist(c);
        return c;
    }

}