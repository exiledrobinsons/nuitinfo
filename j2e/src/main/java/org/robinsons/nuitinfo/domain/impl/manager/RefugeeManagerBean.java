package org.robinsons.nuitinfo.domain.impl.manager;

import org.robinsons.nuitinfo.domain.interfaces.finder.CampFinder;
import org.robinsons.nuitinfo.domain.interfaces.finder.RefugeeFinder;
import org.robinsons.nuitinfo.domain.interfaces.manager.RefugeeManager;
import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.entities.Refugee;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownRefugeeException;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.GregorianCalendar;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class RefugeeManagerBean implements RefugeeManager {
    @PersistenceContext
    private EntityManager manager;

    @EJB
    private CampFinder campFinder;

    @EJB
    private RefugeeFinder refugeeFinder;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Refugee createRefugee(String firstname, String lastname, long campId, String homeCountry, GregorianCalendar birthDate)throws UnknownCampException {
        Camp c = campFinder.findById(campId);

        Refugee r = new Refugee(0,c, firstname, lastname, homeCountry, birthDate);
        manager.persist(r);
        manager.merge(c);
        return r;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Refugee changeCampId(long refugeeId, long campId) throws UnknownCampException, UnknownRefugeeException {
        Refugee r = this.refugeeFinder.findById(refugeeId);
        Camp c = campFinder.findById(campId);
        manager.merge(r);
        manager.merge(c);
        return r;
    }
}
