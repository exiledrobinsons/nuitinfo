package org.robinsons.nuitinfo.domain.interfaces.finder;

import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;

public interface CampFinder {
    public Camp findById(long id)throws UnknownCampException;

    boolean exists(long id);
}
