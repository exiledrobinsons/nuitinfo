package org.robinsons.nuitinfo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Camp implements Serializable{
    private long id;

    private String name;

    private List<Refugee> refugees;

    private Double longitude;

    private Double latitude;

    private String pays;

    public Camp() {
        this.name = "";
        this.latitude = 0.;
        this.longitude = 0.;
        this.refugees = new ArrayList<Refugee>();

    }

    public Camp(String name, Double latitude, Double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.refugees = new ArrayList<Refugee>();
    }

    public Camp(long id, String rescueCamp,  Double latitude, Double longitude, String pays) {
        this.name = rescueCamp;
        this.latitude = latitude;
        this.longitude = longitude;
        this.refugees = new ArrayList<Refugee>();
        this.pays = pays;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Refugee> getRefugee() {
        return refugees;
    }

    public void setRefugee(List<Refugee> refugees) {
        this.refugees = refugees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Refugee> getRefugees() {
        return refugees;
    }

    public void setRefugees(List<Refugee> refugees) {
        this.refugees = refugees;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }
}
