package org.robinsons.nuitinfo.entities;

import javax.persistence.*;
import java.util.GregorianCalendar;

public class CampMission extends Mission{

    private Camp camp;

    public CampMission() {
        this.camp = new Camp();
    }


    public CampMission(String name, String missionDetails, GregorianCalendar startDate, GregorianCalendar endDate, Camp camp) {
        this.setName(name);
        this.setMissionDetails(missionDetails);
        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.camp = new Camp();
    }
    public CampMission(long id,String name, String missionDetails, GregorianCalendar startDate, GregorianCalendar endDate, Camp camp) {
        this.setId(id);
        this.setName(name);
        this.setMissionDetails(missionDetails);
        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.camp = camp;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }
}
