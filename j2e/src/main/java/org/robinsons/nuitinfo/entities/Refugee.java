package org.robinsons.nuitinfo.entities;

import java.io.Serializable;
import java.util.GregorianCalendar;

public class Refugee implements Serializable {
    private long id;

    private Camp camp;

    private String firstname;

    private String lastname;

    private String homeCountry;

    private String sexe;

    private GregorianCalendar birthDate;

    public Refugee() {
    }

    public Refugee(long id, Camp camp, String firstname, String lastname, String homeCountry, GregorianCalendar birthDate) {
        this.setId(id);
        this.camp = camp;
        this.firstname = firstname;
        this.lastname = lastname;
        this.homeCountry = homeCountry;
        this.birthDate = birthDate;
    }

    public Refugee(long id, Camp camp, String firstname, String lastname, String homeCountry, GregorianCalendar birthDate, String sexe) {
        this.id = id;
        this.camp = camp;
        this.firstname = firstname;
        this.lastname = lastname;
        this.homeCountry = homeCountry;
        this.birthDate = birthDate;
        this.sexe = sexe;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getHomeCountry() {
        return homeCountry;
    }

    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    public GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(GregorianCalendar birthDate) {
        this.birthDate = birthDate;
    }

    public Camp getCamp() {
        return camp;
    }

    public void setCamp(Camp camp) {
        this.camp = camp;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
}
