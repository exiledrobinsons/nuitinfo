package org.robinsons.nuitinfo.presentation;

import org.json.JSONException;
import org.robinsons.nuitinfo.domain.DataAccessObject;
import org.robinsons.nuitinfo.presentation.utils.TimelineJSONWriter;

/**
 * Created by Benjamin Bourgeois on 05/12/2014 02:31.
 */
public class Main {

    public static void main(String[] args) throws JSONException{

        DataAccessObject dao = new DataAccessObject();

        TimelineJSONWriter.writeTimelineJSON("src/main/webapp/resources/json/timeline_nuit_info.json", dao.getMissions());

    }
}
