package org.robinsons.nuitinfo.presentation;

import org.robinsons.nuitinfo.domain.interfaces.finder.MissionFinder;
import org.robinsons.nuitinfo.domain.interfaces.manager.MissionManager;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;

/**
 * Created by Benjamin Bourgeois on 04/12/2014 22:53.
 */
@ManagedBean
public class PresentationJsfBean {
//    @EJB private MissionFinder missionFinder;
//    @EJB private MissionManager missionManager;

    private boolean actionsRendered;
    private boolean donationsRendered;
    private boolean preventionRendered;

    public static boolean actionsRenderedStatic;
    public static boolean donationsRenderedStatic;
    public static boolean preventionRenderedStatic;

    public boolean isActionsRendered() {
        System.out.println(this);
        return actionsRendered;
    }

    public void setActionsRendered(boolean actionsRendered) {
        this.actionsRendered = actionsRendered;
    }

    public boolean isDonationsRendered() {
        System.out.println(this);
        return donationsRendered;
    }

    public void setDonationsRendered(boolean donationsRendered) {
        this.donationsRendered = donationsRendered;
    }

    public boolean isPreventionRendered() {
        System.out.println(this);
        return preventionRendered;
    }

    public void setPreventionRendered(boolean preventionRendered) {
        this.preventionRendered = preventionRendered;
    }

    public void toggleOnActionsRendered() {
        this.preventionRendered = false;
        this.donationsRendered = false;
        this.actionsRendered = true;
        setPreventionRenderedStatic(false);
        setDonationsRenderedStatic(false);
        setActionsRenderedStatic(true);
        System.out.println(this);
    }

    public void toggleOnDonationsRendered() {
        this.actionsRendered = false;
        this.preventionRendered = false;
        this.donationsRendered = true;
        setPreventionRenderedStatic(false);
        setActionsRenderedStatic(false);
        setDonationsRenderedStatic(true);
        System.out.println(this);
    }

    public void toggleOnPreventionRendered() {
        this.actionsRendered = false;
        this.donationsRendered = false;
        this.preventionRendered = true;
        setDonationsRenderedStatic(false);
        setActionsRenderedStatic(false);
        setPreventionRenderedStatic(true);
        System.out.println(this);
    }

    public boolean isActionsRenderedStatic() {
        return actionsRenderedStatic;
    }

    public boolean isDonationsRenderedStatic() {
        return donationsRenderedStatic;
    }

    public boolean isPreventionRenderedStatic() {
        return preventionRenderedStatic;
    }

    public void setActionsRenderedStatic(boolean actionsRenderedStatic) {
        PresentationJsfBean.actionsRenderedStatic = actionsRenderedStatic;
    }

    public void setDonationsRenderedStatic(boolean donationsRenderedStatic) {
        PresentationJsfBean.donationsRenderedStatic = donationsRenderedStatic;
    }

    public void setPreventionRenderedStatic(boolean preventionRenderedStatic) {
        PresentationJsfBean.preventionRenderedStatic = preventionRenderedStatic;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PresentationJsfBean{");
        sb.append("actionsRendered=").append(actionsRendered);
        sb.append(", donationsRendered=").append(donationsRendered);
        sb.append(", preventionRendered=").append(preventionRendered);
        sb.append(",actionsRenderedStatic=").append(actionsRenderedStatic);
        sb.append(", donationsRenderedStatic=").append(donationsRenderedStatic);
        sb.append(", preventionRenderedStatic=").append(preventionRenderedStatic);
        sb.append('}');
        return sb.toString();
    }
}
