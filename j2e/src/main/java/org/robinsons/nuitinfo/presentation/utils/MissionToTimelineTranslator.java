package org.robinsons.nuitinfo.presentation.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.robinsons.nuitinfo.entities.CampMission;
import org.robinsons.nuitinfo.entities.Mission;

import java.util.*;

/**
 * Created by Benjamin Bourgeois on 05/12/2014 05:32.
 */
public class MissionToTimelineTranslator {

    public static List<JSONObject> convertMissionsToTimelineEvent(List<Mission> missions) {
        List<JSONObject> list = new ArrayList<>();
        for(Mission mission : missions) {
            list.add(convertMissionToTimelineEvent(mission));
        }
        return list;
    }

    private static JSONObject convertMissionToTimelineEvent(Mission mission) {
        CampMission campMission = (CampMission) mission;
        JSONObject asset, date = null;
        try {
            date = new JSONObject();
            // start date
            GregorianCalendar cal = campMission.getStartDate();
            date.put("startDate", cal.get(Calendar.YEAR) + "," + (cal.get(Calendar.MONTH)+1) + "," + cal.get(Calendar.DAY_OF_MONTH));
            // end date
            cal = campMission.getEndDate();
            date.put("endDate", cal.get(Calendar.YEAR)+","+(cal.get(Calendar.MONTH)+1)+","+cal.get(Calendar.DAY_OF_MONTH));
            date.put("headline", mission.getName() + " at " + campMission.getCamp().getName());
            date.put("text", "<p>Country: " + campMission.getCamp().getPays() + "</p><p>" + campMission.getMissionDetails() + "</p>");
//            date.put("tag", ticket.getCategory());
            asset = new JSONObject();
            asset.put("media", "https://www.google.fr/maps/@" + campMission.getCamp().getLatitude() + "," + campMission.getCamp().getLongitude() + ",9z");
            asset.put("caption", "Mission location");
            asset.put("credit", "");
            date.put("asset", asset);
        } catch (JSONException e) {
            System.err.println("An error occurred when converting a mission to a timeline event: "+e.getMessage());
        }
        return date;
    }

}
