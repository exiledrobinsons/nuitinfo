package org.robinsons.nuitinfo.presentation.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.robinsons.nuitinfo.entities.Mission;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Benjamin Bourgeois on 05/12/2014 02:21.
 */
public class TimelineJSONWriter {

    public static JSONObject writeTimelineJSON(String fileNameTarget, List<Mission> missions) throws JSONException {

        JSONObject timeline = new JSONObject();
        // header
        timeline.put("headline","\"La nuit de l'info au service de l'humanitaire !\"");
        timeline.put("type","default");
        timeline.put("text","<p>This timeline present  our actions all around the world.</p>");
        JSONObject asset = new JSONObject();
        asset.put("media","resources/img/robinson.png");
        asset.put("credit","Les Robinsons");
        asset.put("caption","");
        timeline.put("asset", asset);
        // events
        JSONArray dates = new JSONArray();
        // retrieve the google map
//        JSONObject date = new JSONObject();
//        date.put("headline", "Mission 1");
//        date.put("text", "Ceci est le descriptif de la mission 1");
//        date.put("startDate", "2014,12,05");
//        date.put("endDate", "2014,12,06");
//        asset = new JSONObject();
//        asset.put("media", "https://www.google.fr/maps/@43.9207284,7.1772024,9z");
//        date.put("asset", asset);
        // put the event
        List<JSONObject> eventsMissions = MissionToTimelineTranslator.convertMissionsToTimelineEvent(missions);
        for(JSONObject jsonObject : eventsMissions) {
            dates.put(jsonObject);
        }
        // put the events
        timeline.put("date", dates);

        // writing json file
        try {
            JSONObject toWrite = new JSONObject();
            toWrite.put("timeline", timeline);
            FileWriter file = new FileWriter(fileNameTarget);
            file.write(toWrite.toString());
            file.flush();
            file.close();
            return toWrite;
        } catch (IOException e) {
            System.err.println("There was an error writing the JSON file for the timeline.");
            e.printStackTrace();
        }
        return null;
    }

}
