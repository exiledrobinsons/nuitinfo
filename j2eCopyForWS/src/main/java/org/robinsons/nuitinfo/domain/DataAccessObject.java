package org.robinsons.nuitinfo.domain;

import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.entities.CampMission;
import org.robinsons.nuitinfo.entities.Mission;
import org.robinsons.nuitinfo.entities.Refugee;

import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

@Singleton(name = "RobinsonsDatabaseMOC")
public class DataAccessObject {

    private List<Mission> missions;
    private List<Camp> camps;
    private List<Refugee> refugees;

    public DataAccessObject() {
        initCamps();
        initMissions();
        initRefugees();
    }

    private void initRefugees() {
        GregorianCalendar d1 = new GregorianCalendar();
        d1.set(1994,12, 25);
        GregorianCalendar d2 = new GregorianCalendar();
        d2.set(1995,1, 5);
        GregorianCalendar d3 = new GregorianCalendar();
        d3.set(1995,2, 2);
        GregorianCalendar d4 = new GregorianCalendar();
        d4.set(1995,3, 25);
        GregorianCalendar d5 = new GregorianCalendar();
        d5.set(1995,5, 2);
        GregorianCalendar d6 = new GregorianCalendar();
        d6.set(1995,7, 5);
        this.refugees = new ArrayList<>(Arrays.asList(
                new Refugee(0,this.camps.get(0), "Emily", "Pope", "Grece",d1 ),
                new Refugee(1,this.camps.get(0), "Elisa", "Pinsard", "Brasil",d2 ),
                new Refugee(2,this.camps.get(2), "Mariuska", "Salsa", "Venezuella",d3 ),
                new Refugee(3,this.camps.get(0), "Joe", "Enrike", "Chili",d4 ),
                new Refugee(4,this.camps.get(3), "Pablo", "Ricardo", "Argentine",d5 ),

                new Refugee(6,this.camps.get(0), "Robert", "Bob", "Brasil",d2 ),
                new Refugee(7,this.camps.get(2), "A", "A", "Venezuella",d3 ),
                new Refugee(8,this.camps.get(0), "B", "B", "Chili",d4 ),
                new Refugee(9,this.camps.get(3), "C", "C", "Argentine",d5 ),

                new Refugee(5,this.camps.get(3), "Ibraime", "Zoulou", "Togo",d6 )
        ));
        for (Refugee r : this.refugees){
            r.getCamp().getRefugees().add(r);
        }

    }

    private void initCamps() {
        this.camps = new ArrayList<>(Arrays.asList(
                new Camp(0,"RescueCamp", 12.3656600, -1.5338800, "Burkina Faso"),
                new Camp(1,"MugCamp", 13.3656600, -12.5338800, "Irak"),
                new Camp(2,"PenCamp", 21.3656600, -13.5338800, "Togo"),
                new Camp(3,"KeyCamp", 16.3656600, -14.5338800, "Nigeria"),
                new Camp(5,"HelpCamp", 12.3656600, -1.5338800, "Burkina Faso"),
                new Camp(6,"PaperCamp", 13.3656600, -12.5338800, "Irak"),
                new Camp(7,"ACamp", 21.3656600, -13.5338800, "Togo"),
                new Camp(8,"SuperCamp", 16.3656600, -14.5338800, "Nigeria"),
                new Camp(9,"BootCamp", 18.3656600, 14.5338800, "South Africa"),
                new Camp(4,"BottleCamp", 19.3656600, -34.5338800, "Kenya")
        ));
    }
    private void initMissions() {
        GregorianCalendar d1 = new GregorianCalendar();
        d1.set(2012,12, 25);
        GregorianCalendar d2 = new GregorianCalendar();
        d2.set(2013,1, 5);
        GregorianCalendar d4 = new GregorianCalendar();
        d4.set(2013,3, 25);
        GregorianCalendar d6 = new GregorianCalendar();
        d6.set(2013,6, 5);
        GregorianCalendar d7 = new GregorianCalendar();
        d6.set(2013,7, 6);
        GregorianCalendar d10 = new GregorianCalendar();
        d6.set(2013,7, 18);
        GregorianCalendar d9 = new GregorianCalendar();
        d6.set(2013,7, 21);
        GregorianCalendar d8 = new GregorianCalendar();
        d6.set(2013,10, 5);
        GregorianCalendar d3 = new GregorianCalendar();
        d3.set(2014,2, 2);
        GregorianCalendar d5 = new GregorianCalendar();
        d5.set(2014,5, 2);
        this.missions = new ArrayList<Mission>(Arrays.asList(
                new CampMission(0,"rescue", "save people",d1, d2, this.camps.get(0) ),
                new CampMission(1,"save", "rescue people",d4, d3, this.camps.get(1) ),
                new CampMission(2,"rescue", "birth people",d1, d4, this.camps.get(2) ),
                new CampMission(3,"heal", "revive people",d6, d5, this.camps.get(3) ),
                new CampMission(4,"life", "make people alive",d4, d6, this.camps.get(4)),
                new CampMission(5,"heal", "revive people",d7, d5, this.camps.get(5) ),
                new CampMission(6,"life", "make people alive",d4, d9, this.camps.get(6)),
                new CampMission(7,"heal", "revive people",d4, d8, this.camps.get(7) ),
                new CampMission(8,"life", "make people alive",d7, d10, this.camps.get(8) )
        ));
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }

    public List<Camp> getCamps() {
        return camps;
    }

    public void setCamps(List<Camp> camps) {
        this.camps = camps;
    }

    public List<Refugee> getRefugees() {
        return refugees;
    }

    public void setRefugees(List<Refugee> refugees) {
        this.refugees = refugees;
    }

    public List<Refugee> getListRefugeeByName(String firstname, String lastname){
        List<Refugee> refugees = new ArrayList<Refugee>();
        for(Refugee r : this.refugees){
            if(r.getFirstname().contains(firstname) || r.getLastname().contains(lastname) || r.getFirstname().contains(lastname) || r.getLastname().contains(firstname)){
                refugees.add(r);
            }
        }

        return refugees;
    }

    public Refugee getListRefugeeById(Long id){
        for(Refugee r : this.refugees){
            if(r.getId()==id){
                return r;
            }
        }

        return null;
    }


}
