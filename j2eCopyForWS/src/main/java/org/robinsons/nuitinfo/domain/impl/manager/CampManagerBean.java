package org.robinsons.nuitinfo.domain.impl.manager;

import org.robinsons.nuitinfo.domain.interfaces.manager.CampManager;
import org.robinsons.nuitinfo.entities.Camp;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CampManagerBean implements CampManager{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Camp createCamp(String name, Double latitude, Double longitude){
        Camp c = new Camp(name, latitude, longitude);
        entityManager.persist(c);
        return c;
    }
    /*
		@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Channel addMember(String name, String login, UserRights userRight)
			throws UnknownUserException, UnknownChannelException, NotAllowedOperationException {
		User u = userFinder.findByLogin(login);
		Channel c = channelFinder.findByName(name);

		for(UserChannel uc : c.members){
			if(uc.userLogin.equals(u.login) && uc.channelName.equals(c.name)) {
				throw new NotAllowedOperationException("User "+ u.login +" already in channel "+c.name);
			}
		}

		c.add(u, userRight);
		manager.merge(c);
		manager.merge(u);
		return c;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Channel removeMember(String name, String login)
			throws UnknownEntityException, NotAllowedOperationException {
		User u = userFinder.findByLogin(login);
		Channel c = channelFinder.findByName(name);
		UserChannel uc = channelFinder.findUserChannel(login, name);

		c.remove(u, uc);
		manager.merge(c);
		manager.merge(u);
		manager.remove(uc);
		return c;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Channel changeUserRights(String name, String login,
			UserRights userRight) throws UnknownEntityException,
			NotAllowedOperationException {
		this.removeMember(name, login);
		return this.addMember(name, login, userRight);
	}
    */

}
