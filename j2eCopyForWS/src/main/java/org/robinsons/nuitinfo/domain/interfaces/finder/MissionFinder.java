package org.robinsons.nuitinfo.domain.interfaces.finder;

import org.robinsons.nuitinfo.entities.Mission;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

public interface MissionFinder{
    public Mission findByName(String n);

    public List<Mission> getAllMission();

    boolean exists(String name);
}
