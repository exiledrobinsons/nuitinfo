package org.robinsons.nuitinfo.domain.interfaces.finder;

import org.robinsons.nuitinfo.entities.Refugee;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownRefugeeException;

import java.util.List;

public interface RefugeeFinder {
    /**
     * Find a refugee in the database using his id
     *
     * @param id
     *            the id of the refugee
     * @return the refugee
     */
    public Refugee findById(long id) throws UnknownRefugeeException;

    /**
     * Find a refugee in the database using his first and last name.
     *
     * @param firstname
     *            the firstname of the refugee
     * @param lastname
     *            the lastname of the refugee
     * @return the refugee
     */
    public Refugee findByName(String firstname, String lastname) throws UnknownRefugeeException;

    /**
     * Find a refugee in the database using his first and last name.
     *
     * @param firstname
     *            the firstname of the refugee
     * @param lastname
     *            the lastname of the refugee
     * @return the refugee
     */
    public List<Refugee> findRefugeesByName(String firstname, String lastname) throws UnknownRefugeeException;

    /**
     * Check if a refugee exist in the database
     *
     * @param id
     *            the id of the refugee
     * @return true if he exist, false otherwise
     */
    public boolean exists(long id);
}
