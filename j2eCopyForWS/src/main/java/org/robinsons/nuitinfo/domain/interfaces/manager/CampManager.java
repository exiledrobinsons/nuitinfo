package org.robinsons.nuitinfo.domain.interfaces.manager;

import org.robinsons.nuitinfo.entities.Camp;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

public interface CampManager {

    public Camp createCamp(String name, Double latitude, Double longitude);
}
