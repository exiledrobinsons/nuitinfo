package org.robinsons.nuitinfo.domain.interfaces.manager;

import org.robinsons.nuitinfo.entities.CampMission;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;

import java.util.GregorianCalendar;

public interface MissionManager {

    public CampMission createCampMission(String name, String missionDetails, GregorianCalendar startDate, GregorianCalendar endDate, long campId) throws UnknownCampException;


}
