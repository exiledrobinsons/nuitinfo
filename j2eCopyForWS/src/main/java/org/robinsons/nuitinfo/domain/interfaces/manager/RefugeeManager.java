package org.robinsons.nuitinfo.domain.interfaces.manager;

import org.robinsons.nuitinfo.entities.Refugee;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownCampException;
import org.robinsons.nuitinfo.exceptions.unknown.UnknownRefugeeException;

import java.util.GregorianCalendar;

public interface RefugeeManager {
    /**
     * Create a new refugee. A correct campId is requested in order to create a new refugee.
     *
     * @param firstname   firstname of this refugee
     * @param lastname    lastname of this refugee
     * @param campId      campId where the refugee is registered
     * @param homeCountry homeCountry of this refugee
     * @param birthDate   birthdate of this refugee
     * @return the created refugee
     */
    public Refugee createRefugee(String firstname, String lastname, long campId, String homeCountry, GregorianCalendar birthDate) throws UnknownCampException;

    /**
     * Allow to change campId for refugeeId. campId can be equals to -1 if the refugee is leaving the camp.
     *
     * @param refugeeId the refugee Id
     * @param campId    the newest campId or -1 if he is leaving the camp
     * @return the Refugee
     */
    public Refugee changeCampId(long refugeeId, long campId) throws UnknownCampException, UnknownRefugeeException;
}
