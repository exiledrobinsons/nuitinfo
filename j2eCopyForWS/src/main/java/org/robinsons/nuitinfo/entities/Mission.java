package org.robinsons.nuitinfo.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public abstract class Mission implements Serializable{
    private long id;

    private String missionDetails;

    private List<Humanitarian> humanitarians;

    private String name;

    private GregorianCalendar startDate;

    private GregorianCalendar endDate;

    public Mission(){
        this.missionDetails = "";
        this.humanitarians = new ArrayList<Humanitarian>();
        this.name="";
        this.startDate = new GregorianCalendar();
        this.endDate = new GregorianCalendar();
    }


    public long getId() {
        return id;
    }

    public String getMissionDetails() {
        return missionDetails;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMissionDetails(String missionDetails) {
        this.missionDetails = missionDetails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<Humanitarian> getFieldAgents() {
        return humanitarians;
    }

    public void setFieldAgents(List<Humanitarian> humanitarians) {
        this.humanitarians = humanitarians;
    }

    public GregorianCalendar getStartDate() {
        return startDate;
    }

    public void setStartDate(GregorianCalendar startDate) {
        this.startDate = startDate;
    }

    public GregorianCalendar getEndDate() {
        return endDate;
    }

    public void setEndDate(GregorianCalendar endDate) {
        this.endDate = endDate;
    }

    public List<Humanitarian> getHumanitarians() {
        return humanitarians;
    }

    public void setHumanitarians(List<Humanitarian> humanitarians) {
        this.humanitarians = humanitarians;
    }
}
