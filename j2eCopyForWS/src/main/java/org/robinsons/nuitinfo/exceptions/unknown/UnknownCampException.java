package org.robinsons.nuitinfo.exceptions.unknown;

public class UnknownCampException extends UnknownEntityException {
    private static final long serialVersionUID = 1L;

    public UnknownCampException(String camp) {
        super("camp: " + camp);
    }
}
