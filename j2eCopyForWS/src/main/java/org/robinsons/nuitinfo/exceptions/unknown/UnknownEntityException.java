package org.robinsons.nuitinfo.exceptions.unknown;

import javax.xml.soap.SOAPException;

public class UnknownEntityException extends SOAPException {

    private static final long serialVersionUID = 1L;

    public UnknownEntityException(String m){
        super("Unknown "+m);
    }
}
