package org.robinsons.nuitinfo.exceptions.unknown;

public class UnknownRefugeeException extends UnknownEntityException {
    private static final long serialVersionUID = 1L;

    public UnknownRefugeeException(String refuge) {
        super("refugee: " + refuge);
    }
}
