package org.robinsons.nuitinfo.webservice;

import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Produces({"application/xml"})
public interface RobinsonWS {

    @Path("/refugee")
    @GET
    public Response refugee(@QueryParam("name") String name,
                            @QueryParam("homeCountry") String homeCountry,
                            @QueryParam("country") String country);

    @Path("/refugee/idList")
    @GET
    public Response refugee(@QueryParam("idList") String jsonIdList);
}
