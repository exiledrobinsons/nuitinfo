package org.robinsons.nuitinfo.webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.robinsons.nuitinfo.domain.DataAccessObject;
import org.robinsons.nuitinfo.entities.Camp;
import org.robinsons.nuitinfo.entities.Refugee;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/rest/robinson")
public class RobinsonWSImpl implements RobinsonWS {

    @EJB
    private DataAccessObject dao;

    @Override
    public Response refugee(String name, String homeCountry, String country) {
        try {
            if (!name.equals("")) {
                String[] names = name.split(" ");

                List<Refugee> refugeeList = dao.getListRefugeeByName(names[0], names[1]);
                JSONArray jsonArray = new JSONArray();
                for (Refugee refugee : refugeeList) {
                    JSONObject json = new JSONObject();
                    json.put("firstname", refugee.getFirstname());
                    json.put("lastname", refugee.getLastname());
                    json.put("homeCountry", refugee.getHomeCountry());
                    json.put("id", refugee.getId());
                    json.put("sexe", refugee.getSexe());
                    JSONObject jsonCamp = new JSONObject();
                    Camp c = refugee.getCamp();
                    jsonCamp.put("name", c.getName());
                    jsonCamp.put("longitude", c.getLongitude());
                    jsonCamp.put("latitude", c.getLatitude());
                    jsonCamp.put("country", c.getPays());
                    json.put("camp", jsonCamp);
                    jsonArray.put(json);
                }
                return Response.ok(jsonArray.toString()).header("Content-Type", "application/json").build();

            } else {
                List<Refugee> refugeeList = dao.getRefugees();
                JSONArray jsonArray = new JSONArray();
                for (Refugee refugee : refugeeList) {
                    JSONObject json = new JSONObject();
                    json.put("firstname", refugee.getFirstname());
                    json.put("lastname", refugee.getLastname());
                    json.put("homeCountry", refugee.getHomeCountry());
                    json.put("id", refugee.getId());
                    json.put("sexe", refugee.getSexe());
                    JSONObject jsonCamp = new JSONObject();
                    Camp c = refugee.getCamp();
                    jsonCamp.put("name", c.getName());
                    jsonCamp.put("longitude", c.getLongitude());
                    jsonCamp.put("latitude", c.getLatitude());
                    jsonCamp.put("country", c.getPays());
                    json.put("camp", jsonCamp);
                    jsonArray.put(json);
                }
                return Response.ok(jsonArray.toString()).header("Content-Type", "application/json").build();
            }
        } catch (JSONException e) {
            return Response.noContent().status(404).build();
        }
    }

    @Override
    public Response refugee(String jsonIdList) {
        try {
            JSONArray jsonArray0 = new JSONArray(jsonIdList);

            List<Refugee> refugeeList = new ArrayList<Refugee>();

            for (int i = 0; i < jsonArray0.length() - 1; i++) {
                long id = jsonArray0.getLong(i);
                Refugee r = dao.getListRefugeeById(id);
                if (r != null) {
                    refugeeList.add(r);
                }
            }

            JSONArray jsonArray = new JSONArray();
            for (Refugee refugee : refugeeList) {
                JSONObject json = new JSONObject();
                json.put("firstname", refugee.getFirstname());
                json.put("lastname", refugee.getLastname());
                json.put("homeCountry", refugee.getHomeCountry());
                json.put("id", refugee.getId());
                json.put("sexe", refugee.getSexe());
                JSONObject jsonCamp = new JSONObject();
                Camp c = refugee.getCamp();
                jsonCamp.put("name", c.getName());
                jsonCamp.put("longitude", c.getLongitude());
                jsonCamp.put("latitude", c.getLatitude());
                jsonCamp.put("country", c.getPays());
                json.put("camp", jsonCamp);
                jsonArray.put(json);
            }
            return Response.ok(jsonArray.toString()).header("Content-Type", "application/json").build();
        } catch (JSONException e) {
            return Response.noContent().status(404).build();
        }
    }
}
